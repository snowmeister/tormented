﻿#pragma strict


var breathSound: AudioClip; // assign the breath sound in the Inspector
var walkRate: float = 4.0; // breath interval when walking
var walkSpeed: float = 1.5; // is walking when above this speed
var runRate: float = 2.0; // breath interval when running
var runSpeed: float = 3.5; // is running when above this speed


private var lastPos: Vector3;
private var nextBreath: float = 0;
private var interval: float = 1; // breath interval - 0 means no breath sound


function Update() {
    var dt = Time.deltaTime;
    if (dt > 0) { // only breathe when game not paused!
        // calculate the distance since last frame:
        var dist = Vector3.Distance(lastPos, transform.position);
        // update lastPos:
        lastPos = transform.position;
        // calculate the current speed:
        var speed = dist / dt;
        if (speed > runSpeed) { // is running?
            interval = runRate;
        } else
        if (speed > walkSpeed) { // else is walking?
            interval = walkRate;
        } else { // else stop breath sound
            interval = 0;
        }
        // if breath enabled and it's time to breath...
        if (interval > 0 && Time.time > nextBreath) {
            Debug.Log('breath');
            GetComponent.<AudioSource>().PlayOneShot(breathSound); // play the sound...
            nextBreath = Time.time + interval; // and set next time to breath
        }
    }
}
