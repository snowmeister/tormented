//uScript Generated Code - Build 1.0.3055
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("PlayerScript", "The main controller script for the player gameobject")]
public class Player3 : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.Single local_negativeMoveAmount_System_Single = (float) -0.1;
   System.Single local_positiveMoveAmount_System_Single = (float) 0.1;
   public System.Single negativerotatespeed = (float) -1;
   public System.Single positiveMoveRunSpeed = (float) 0.5;
   public System.Single positiverotatespeed = (float) 1;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_3 = null;
   UnityEngine.GameObject owner_Connection_6 = null;
   UnityEngine.GameObject owner_Connection_10 = null;
   UnityEngine.GameObject owner_Connection_12 = null;
   UnityEngine.GameObject owner_Connection_17 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_1 = UnityEngine.KeyCode.W;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_1 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_5 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_5 = UnityEngine.KeyCode.S;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_5 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_5 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_5 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_8 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_8 = UnityEngine.KeyCode.A;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_8 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_8 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_8 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_9 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_9 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_9 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_9 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_9 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_9 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_9 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_9 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_9 = UnityEngine.Space.Self;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_9 = (bool) false;
   bool logic_uScriptAct_SetGameObjectRotation_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_11 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_11 = UnityEngine.KeyCode.D;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_11 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_11 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_11 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectRotation logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_13 = new uScriptAct_SetGameObjectRotation( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectRotation_Target_13 = new UnityEngine.GameObject[] {};
   System.Single logic_uScriptAct_SetGameObjectRotation_XDegrees_13 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_YDegrees_13 = (float) 0;
   System.Single logic_uScriptAct_SetGameObjectRotation_ZDegrees_13 = (float) 0;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreX_13 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreY_13 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_IgnoreZ_13 = (bool) false;
   UnityEngine.Space logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_13 = UnityEngine.Space.Self;
   System.Boolean logic_uScriptAct_SetGameObjectRotation_AsOffset_13 = (bool) false;
   bool logic_uScriptAct_SetGameObjectRotation_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_15 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_15 = UnityEngine.KeyCode.Space;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_15 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_15 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_15 = true;
   //pointer to script instanced logic node
   uScriptAct_AddForce logic_uScriptAct_AddForce_uScriptAct_AddForce_16 = new uScriptAct_AddForce( );
   UnityEngine.GameObject logic_uScriptAct_AddForce_Target_16 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_AddForce_Force_16 = new Vector3( (float)0, (float)500, (float)0 );
   System.Single logic_uScriptAct_AddForce_Scale_16 = (float) 10;
   System.Boolean logic_uScriptAct_AddForce_UseForceMode_16 = (bool) false;
   UnityEngine.ForceMode logic_uScriptAct_AddForce_ForceModeType_16 = UnityEngine.ForceMode.Force;
   bool logic_uScriptAct_AddForce_Out_16 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_19 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_19 = UnityEngine.KeyCode.UpArrow;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_19 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_19 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_19 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_20 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_20 = UnityEngine.KeyCode.DownArrow;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_20 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_20 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_20 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_21 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_21 = UnityEngine.KeyCode.LeftArrow;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_21 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_21 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_21 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_22 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_22 = UnityEngine.KeyCode.RightArrow;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_22 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_22 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_22 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_27 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_27 = UnityEngine.KeyCode.LeftShift;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_27 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_27 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_27 = true;
   //pointer to script instanced logic node
   uScriptAct_SetFloat logic_uScriptAct_SetFloat_uScriptAct_SetFloat_28 = new uScriptAct_SetFloat( );
   System.Single logic_uScriptAct_SetFloat_Value_28 = (float) 0;
   System.Single logic_uScriptAct_SetFloat_Target_28;
   bool logic_uScriptAct_SetFloat_Out_28 = true;
   //pointer to script instanced logic node
   uScriptAct_SetFloat logic_uScriptAct_SetFloat_uScriptAct_SetFloat_30 = new uScriptAct_SetFloat( );
   System.Single logic_uScriptAct_SetFloat_Value_30 = (float) 0.1;
   System.Single logic_uScriptAct_SetFloat_Target_30;
   bool logic_uScriptAct_SetFloat_Out_30 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_2 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_2 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_2 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_4 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_4 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_4 = (float) 0;
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_3 || false == m_RegisteredForEvents )
      {
         owner_Connection_3 = parentGameObject;
      }
      if ( null == owner_Connection_6 || false == m_RegisteredForEvents )
      {
         owner_Connection_6 = parentGameObject;
      }
      if ( null == owner_Connection_10 || false == m_RegisteredForEvents )
      {
         owner_Connection_10 = parentGameObject;
      }
      if ( null == owner_Connection_12 || false == m_RegisteredForEvents )
      {
         owner_Connection_12 = parentGameObject;
      }
      if ( null == owner_Connection_17 || false == m_RegisteredForEvents )
      {
         owner_Connection_17 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Input component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Input>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Input>();
               }
               if ( null != component )
               {
                  component.KeyEvent += Instance_KeyEvent_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Input component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Input>();
            if ( null != component )
            {
               component.KeyEvent -= Instance_KeyEvent_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_5.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_8.SetParent(g);
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_9.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_11.SetParent(g);
      logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_13.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_15.SetParent(g);
      logic_uScriptAct_AddForce_uScriptAct_AddForce_16.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_19.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_20.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_21.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_22.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_27.SetParent(g);
      logic_uScriptAct_SetFloat_uScriptAct_SetFloat_28.SetParent(g);
      logic_uScriptAct_SetFloat_uScriptAct_SetFloat_30.SetParent(g);
      owner_Connection_3 = parentGameObject;
      owner_Connection_6 = parentGameObject;
      owner_Connection_10 = parentGameObject;
      owner_Connection_12 = parentGameObject;
      owner_Connection_17 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_KeyEvent_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_KeyEvent_0( );
   }
   
   void Relay_KeyEvent_0()
   {
      if (true == CheckDebugBreak("bc017884-2862-48d2-b5b0-42e170084417", "Input_Events", Relay_KeyEvent_0)) return; 
      Relay_In_1();
      Relay_In_5();
      Relay_In_8();
      Relay_In_11();
      Relay_In_15();
      Relay_In_19();
      Relay_In_20();
      Relay_In_21();
      Relay_In_22();
      Relay_In_27();
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("0ff2830b-21ff-4afd-99a5-07f0fb9c748d", "Input_Events_Filter", Relay_In_1)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.In(logic_uScriptAct_OnInputEventFilter_KeyCode_1);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_Translate_2();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("47b94f38-1828-4c17-9de1-1bb80eb96b2e", "UnityEngine_Transform", Relay_Translate_2)) return; 
         {
            {
            }
            {
            }
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_2 = local_positiveMoveAmount_System_Single;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_3.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_2, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_2, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_2);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("9cb76a60-47c8-42e9-8ea7-a4b712680daf", "UnityEngine_Transform", Relay_Translate_4)) return; 
         {
            {
            }
            {
            }
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_4 = local_negativeMoveAmount_System_Single;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_6.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_4, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_4, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_4);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("9043fd53-acff-4823-8154-8368563ff5e1", "Input_Events_Filter", Relay_In_5)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_5.In(logic_uScriptAct_OnInputEventFilter_KeyCode_5);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_5.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_Translate_4();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("96721352-a1ea-4d6a-96a1-2e4671923bde", "Input_Events_Filter", Relay_In_8)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_8.In(logic_uScriptAct_OnInputEventFilter_KeyCode_8);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_8.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("c009168d-8ec3-4ea8-bbb4-a476e68fa62d", "Set_Rotation", Relay_In_9)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetGameObjectRotation_Target_9.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_9, index + 1);
               }
               logic_uScriptAct_SetGameObjectRotation_Target_9[ index++ ] = owner_Connection_10;
               
            }
            {
            }
            {
               logic_uScriptAct_SetGameObjectRotation_YDegrees_9 = negativerotatespeed;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_9.In(logic_uScriptAct_SetGameObjectRotation_Target_9, logic_uScriptAct_SetGameObjectRotation_XDegrees_9, logic_uScriptAct_SetGameObjectRotation_YDegrees_9, logic_uScriptAct_SetGameObjectRotation_ZDegrees_9, logic_uScriptAct_SetGameObjectRotation_IgnoreX_9, logic_uScriptAct_SetGameObjectRotation_IgnoreY_9, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_9, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_9, logic_uScriptAct_SetGameObjectRotation_AsOffset_9);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Set Rotation.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_11()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("08fc1212-42c8-41fb-a55c-5734c7d7a2e8", "Input_Events_Filter", Relay_In_11)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_11.In(logic_uScriptAct_OnInputEventFilter_KeyCode_11);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_11.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_In_13();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_13()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1ca43bef-706f-42c7-a122-18420c544840", "Set_Rotation", Relay_In_13)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetGameObjectRotation_Target_13.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetGameObjectRotation_Target_13, index + 1);
               }
               logic_uScriptAct_SetGameObjectRotation_Target_13[ index++ ] = owner_Connection_12;
               
            }
            {
            }
            {
               logic_uScriptAct_SetGameObjectRotation_YDegrees_13 = positiverotatespeed;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetGameObjectRotation_uScriptAct_SetGameObjectRotation_13.In(logic_uScriptAct_SetGameObjectRotation_Target_13, logic_uScriptAct_SetGameObjectRotation_XDegrees_13, logic_uScriptAct_SetGameObjectRotation_YDegrees_13, logic_uScriptAct_SetGameObjectRotation_ZDegrees_13, logic_uScriptAct_SetGameObjectRotation_IgnoreX_13, logic_uScriptAct_SetGameObjectRotation_IgnoreY_13, logic_uScriptAct_SetGameObjectRotation_IgnoreZ_13, logic_uScriptAct_SetGameObjectRotation_CoordinateSystem_13, logic_uScriptAct_SetGameObjectRotation_AsOffset_13);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Set Rotation.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_15()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("10b92b79-469f-40b1-b378-d326d9a62806", "Input_Events_Filter", Relay_In_15)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_15.In(logic_uScriptAct_OnInputEventFilter_KeyCode_15);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_15.KeyDown;
         
         if ( test_0 == true )
         {
            Relay_In_16();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_16()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("0876394e-76ef-4664-88ee-1129d26c5ac5", "Add_Force", Relay_In_16)) return; 
         {
            {
               logic_uScriptAct_AddForce_Target_16 = owner_Connection_17;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_AddForce_uScriptAct_AddForce_16.In(logic_uScriptAct_AddForce_Target_16, logic_uScriptAct_AddForce_Force_16, logic_uScriptAct_AddForce_Scale_16, logic_uScriptAct_AddForce_UseForceMode_16, logic_uScriptAct_AddForce_ForceModeType_16);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Add Force.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_19()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("45226691-fae1-4023-b141-da4adb7b5a19", "Input_Events_Filter", Relay_In_19)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_19.In(logic_uScriptAct_OnInputEventFilter_KeyCode_19);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_19.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_Translate_2();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_20()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("3803b1ad-510e-4da2-bddb-03a1e76b760e", "Input_Events_Filter", Relay_In_20)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_20.In(logic_uScriptAct_OnInputEventFilter_KeyCode_20);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_20.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_Translate_4();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_21()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("333dee6b-6e6e-4a1f-b092-aab9b79a8419", "Input_Events_Filter", Relay_In_21)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_21.In(logic_uScriptAct_OnInputEventFilter_KeyCode_21);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_21.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_22()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("906391b6-8b5a-4362-b163-ee7239594091", "Input_Events_Filter", Relay_In_22)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_22.In(logic_uScriptAct_OnInputEventFilter_KeyCode_22);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_22.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_In_13();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_27()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("bc761d53-5e6b-4ba7-b665-4d261927b3dc", "Input_Events_Filter", Relay_In_27)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_27.In(logic_uScriptAct_OnInputEventFilter_KeyCode_27);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_27.KeyHeld;
         bool test_1 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_27.KeyUp;
         
         if ( test_0 == true )
         {
            Relay_In_28();
         }
         if ( test_1 == true )
         {
            Relay_In_30();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_28()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("53b04305-8a49-40e9-8dad-2ab43ce69d4d", "Set_Float", Relay_In_28)) return; 
         {
            {
               logic_uScriptAct_SetFloat_Value_28 = positiveMoveRunSpeed;
               
            }
            {
            }
         }
         logic_uScriptAct_SetFloat_uScriptAct_SetFloat_28.In(logic_uScriptAct_SetFloat_Value_28, out logic_uScriptAct_SetFloat_Target_28);
         local_positiveMoveAmount_System_Single = logic_uScriptAct_SetFloat_Target_28;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Set Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_30()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5a453fda-d55f-4f9a-94b9-8151506bd5b2", "Set_Float", Relay_In_30)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_SetFloat_uScriptAct_SetFloat_30.In(logic_uScriptAct_SetFloat_Value_30, out logic_uScriptAct_SetFloat_Target_30);
         local_positiveMoveAmount_System_Single = logic_uScriptAct_SetFloat_Target_30;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Player3.uscript at Set Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Player3.uscript:negativerotatespeed", negativerotatespeed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "b88ffdb0-ae19-4ef2-bea1-0be84b87d7e2", negativerotatespeed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Player3.uscript:positiverotatespeed", positiverotatespeed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "0431e0ca-bddd-46a2-9ec8-3a0a14cb8084", positiverotatespeed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Player3.uscript:positiveMoveAmount", local_positiveMoveAmount_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "df87e9b0-2894-4c83-89ed-f57941fd4bde", local_positiveMoveAmount_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Player3.uscript:negativeMoveAmount", local_negativeMoveAmount_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "21c7b0e3-967d-46b0-82bb-14c2b9aa2d90", local_negativeMoveAmount_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Player3.uscript:positiveMoveRunSpeed", positiveMoveRunSpeed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "6c8f1a74-f33d-47a6-b264-9d34e92e5828", positiveMoveRunSpeed);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
