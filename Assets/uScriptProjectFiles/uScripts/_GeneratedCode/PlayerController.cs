//uScript Generated Code - Build 1.0.3055
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class PlayerController : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   
   //owner nodes
   
   //logic nodes
   
   //event nodes
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   public void Awake()
   {
   }
   
   public void Start()
   {
   }
   
   public void OnEnable()
   {
   }
   
   public void OnDisable()
   {
   }
   
   public void Update()
   {
   }
   
   public void OnDestroy()
   {
   }
   
}
