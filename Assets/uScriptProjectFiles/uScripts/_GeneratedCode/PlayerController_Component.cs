//uScript Generated Code - Build 1.0.3055
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This is the component script that you should assign to GameObjects to use this graph on them. Use the uScript/Graphs section of Unity's "Component" menu to assign this graph to a selected GameObject.

[AddComponentMenu("uScript/Graphs/PlayerController")]
public class PlayerController_Component : uScriptCode
{
   #pragma warning disable 414
   public PlayerController ExposedVariables = new PlayerController( ); 
   #pragma warning restore 414
   
   
   void Awake( )
   {
   }
   void Start( )
   {
   }
   void OnEnable( )
   {
   }
   void OnDisable( )
   {
   }
   void Update( )
   {
   }
   void OnDestroy( )
   {
   }
   #if UNITY_EDITOR
      void OnDrawGizmos( )
      {
      }
   #endif
}
