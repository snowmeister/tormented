[TOC]

# Project Tormented #

The is the repo for the Unity side of Project Tormented. A game I am working on with my son. Nothing to see yet, but watch this space!

## What is this repository for? ##

* This repo will contain all code for the game and will be available to collaborators as we progress
* Current Version: 0.0.1

## What's here so far? ##

To be honest, not much! But, as we progress, that'll soon change. So far we have

### Site structure ###

1. Basic .gitignore setup for Unity projects
1. Basic (but fluid) project directory structure

### Scenes ###

**Mechanisms**

1. breathing_rnd - A scene for investigation/PoC work on the breathing mechanism. So far, not great, but I am intending to solve this with uScript...

**uScript RnD**

1. **uscript1** - Initial dip into uScript, as I am hoping to make my learning curve as shallow as possible, so we achieve as much as possible - as this is HPs first real development project, its important to see results quickly so we don't get frustrated and give up. I hope that by setting small, regular chunks of functionality etc, we should be able to see the project build up in obvious steps. This is the first game project for us both, but using my experience as a professional web developer, I intend to make it fun, and actually deliver a product! This scene simply sets up a VERY basic player controller, where an object can be moved around an area based on keyboard input from the user. Built with no code at all, just graphs in uScript. Clever stuff.

1. **uscript2** -  Building on from uscript1, this adds a basic jump ability, fired when the user hits their keyboard SPACE bar.


### The Wiki 

As this project grows, it's important to keep track of everything, so we've got [the beginnings of a wiki](https://bitbucket.org/snowmeister/tormented/wiki/Home) to document everything. If you spot something missing, let us know!

### The Issue Tracker

Again, as we progress, there will be MANY features to implment, tasks to ensure are carried out, and bugs to sqaush, so we have [an issue tracker](https://bitbucket.org/snowmeister/tormented/issues) ready to go...so, once we get up and running, if you spot something that needs sorting, create a ticket!